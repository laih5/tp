package juego;

import java.awt.Color;

import entorno.Entorno;

public class Fondo {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	
	//Constructor

	public Fondo(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 100;
		this.alto = 200;
		this.color = Color.CYAN;
	}
	
	
	//Dibujar
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}


}
