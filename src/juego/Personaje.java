package juego;

import java.awt.Color;

import entorno.Entorno;

public class Personaje {
	
	//Variables de Instancia 
	private double x;
	private double y;
	private double diametro;
	private Color color;
	
   //Constructor
	public Personaje(double x, double y) {
		this.x = x;
		this.y = y;
		this.diametro = 50;
		this.color = Color.MAGENTA;
	}
	
	
	
	//Metodos
	public void dibujar(Entorno e) {
		e.dibujarCirculo(x, y, diametro, color);
	}

	public void moverDerecha(Entorno e) {
		if (x + diametro/2 < e.ancho()) {
			x = x + 2;
		}
	}

	public void moverIzquierda() {
		if (x - diametro/2 > 0) {
			x = x - 2;
		}
	}

	public void moverArriba() {
		if (y - diametro/2 >0) {
			y = y - 2;
		}
	}
	
	public void moverAbajo(Entorno e) {
		if (y + diametro/2 <e.alto()) {
			y = y + 2;
		}
	}

	public boolean colision(Enemigos[] p){
		for(int i=0;i<p.length;i++) {
		if (x + diametro/2 > p[i].getX() - p[i].getDiametro()/2 && 
			x - diametro/2 < p[i].getX() + p[i].getDiametro()/2 &&
			    y - diametro/2 < p[i].getY() + p[i].getDiametro()/2 && 
				y + diametro/2 > p[i].getY() - p[i].getDiametro()/2) {
			return true;
		}
	}
		return false;
	}

}

