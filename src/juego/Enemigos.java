package juego;

import java.awt.Color;

import entorno.Entorno;

public class Enemigos {
	
	//Variables de instancia
	private double x;
	private double y;
	private double diametro;
	private Color color;
	private int velocidad;
	private double angulo;
	
	//Constructor
	public Enemigos(double x,double y, double angulo) {
		this.x=x;
		this.y=y;
		this.diametro=50;
		this.color = Color.GREEN;
		this.velocidad=2;
		this.angulo=angulo;
	}
	
	//Metodos
	public void dibujar(Entorno e) {
		e.dibujarCirculo(x, y, diametro, color);
	}
	
	public void moverVertical(Entorno e) {
		y = y + Math.cos(angulo)*velocidad;
	}
	
	public void rebotarVertical(Entorno e) {
		if (y + diametro / 2 > e.alto()) {
			angulo = Math.PI - angulo;
		}
		if (y - diametro / 2 < 0) {
			angulo = Math.PI - angulo;
		}
	}
	
	public void moverHorizontal(Entorno e) {
		x = x + Math.cos(angulo)*velocidad;
	}
	
	public void rebotarHorizontal(Entorno e) {
		if (x + diametro / 2 > e.ancho()) {
			angulo = Math.PI - angulo;
		}
		if (x - diametro / 2 < 0) {
			angulo = Math.PI - angulo;
		}
	}
	

	public static void agregarPlanta(Enemigos[] pelotas, Enemigos p) {
		for(int i = 0; i<pelotas.length; i++) {
			if(pelotas[i]==null) {
				pelotas[i] = p;
				return;
			}
		}
	}
	
	public Enemigos crearEnemigo(int x,int y) {
		Enemigos p = new Enemigos(x,y,0);
		return p;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public double getDiametro(){
		return diametro;
	}
}
