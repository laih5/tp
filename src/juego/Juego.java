package juego;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	private Personaje personaje;
	
	private Fondo manzana;
	
	private Enemigos[] plantas;
	private Enemigos planta;
	
	private int distanciaV=25;
	private int distanciaH=150;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Titulo de TP - Grupo N - Apellido1 - Apellido2 -Apellido3 - V0.01", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		personaje= new Personaje(entorno.ancho()/2,570);
		manzana = new Fondo(150,150);
		plantas= new Enemigos[4];
		planta= new Enemigos(50,50,0);
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		personaje.dibujar(entorno);
		manzana.dibujar(entorno);
		
		
		for (int i = 0; i < plantas.length; i++) {
			
			if (i<plantas.length/2) {
				Enemigos.agregarPlanta(plantas, planta.crearEnemigo(distanciaV,50));
				plantas[i].dibujar(entorno);
				plantas[i].moverVertical(entorno);
				plantas[i].rebotarVertical(entorno);
				distanciaV+=400;
			}else{
				Enemigos.agregarPlanta(plantas, planta.crearEnemigo(50,distanciaH));
				plantas[i].dibujar(entorno);
				plantas[i].moverHorizontal(entorno);
				plantas[i].rebotarHorizontal(entorno);
				distanciaH+=200;
			}
			
		}
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			personaje.moverDerecha(entorno);
		}else if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			personaje.moverIzquierda();
		}else if (entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
			personaje.moverArriba();
		}else if (entorno.estaPresionada(entorno.TECLA_ABAJO)) {
			personaje.moverAbajo(entorno);
		}
		
		if (personaje.colision(plantas)) {
			personaje.moverDerecha(entorno);//borrar movimiento y colocar mensaje de JUEGO TERMINADO
		}
		
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
